package ht

type RequestParams struct {
	Header map[string]string
	Query  map[string]string
	Body   map[string]any
}

// Merge 将 local 合并到 global，然后将 custom 合并到 global ，最后返回 global。
func Merge(global RequestParams, local RequestParams, custom RequestParams) RequestParams {
	for k, v := range local.Header {
		global.Header[k] = v
	}
	for k, v := range local.Query {
		global.Query[k] = v
	}
	for k, v := range local.Body {
		global.Body[k] = v
	}

	for k, v := range custom.Header {
		global.Header[k] = v
	}
	for k, v := range custom.Query {
		global.Query[k] = v
	}
	for k, v := range custom.Body {
		global.Body[k] = v
	}
	return global
}

func NewRequestParams() RequestParams {
	return RequestParams{
		Header: make(map[string]string),
		Query:  make(map[string]string),
		Body:   make(map[string]any),
	}
}
