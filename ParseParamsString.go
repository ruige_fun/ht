package ht

import "encoding/json"

type Params struct {
	Header []ParamsItem `json:"header"`
	Query  []ParamsItem `json:"query"`
	Body   []ParamsItem `json:"body"`
}

type ParamsItem struct {
	Id    int    `json:"id"`
	Field string `json:"field"`
	Value string `json:"value"`
}

// ParseParamsString 解析指定格式的参数json，内容格式必须是 Params 的json编码结果。
func ParseParamsString(ps ...string) (RequestParams, error) {
	var rt = NewRequestParams()
	for _, d := range ps {
		var p Params
		err := json.Unmarshal([]byte(d), &p)
		if err != nil {
			return rt, err
		}
		for _, it := range p.Header {
			rt.Header[it.Field] = it.Value
		}
		for _, it := range p.Query {
			rt.Query[it.Field] = it.Value
		}
		for _, it := range p.Body {
			rt.Body[it.Field] = it.Value
		}
	}
	return rt, nil
}
