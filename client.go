package ht

import (
	"fmt"
	"net/http"
	"net/url"
	"time"
)

type HttpClient struct {
	proxyRaw string
	timeout  time.Duration
	ua       UserAgent
	printLog func(a ...any)
}

// NewClient 创建一个http请求的客户端
// proxyRaw 代理信息，空值则不使用代理；非空："socks5://192.168.1.2:1080" "socks5://admin:password@192.168.1.2:8888"
// timeoutSecond 请求超时时间，秒
func NewClient(proxyRaw string, timeoutSecond uint, ua UserAgent) *HttpClient {
	if ua == "" {
		ua = UaPcWinGoogleChrome
	}
	if timeoutSecond <= 0 {
		return &HttpClient{
			proxyRaw: proxyRaw,
			timeout:  0,
			ua:       ua,
		}
	} else {
		return &HttpClient{
			proxyRaw: proxyRaw,
			timeout:  time.Duration(timeoutSecond) * time.Second,
			ua:       ua,
		}
	}
}

// NewProxy 创建一个代理，用于发送请求的时候使用，假如代理链接错误，则返回&http.Transport{},error。
// NewProxy("socks5://127.0.0.1:1080")
// NewProxy("socks5://admin:password@192.168.1.2:8888")
// NewProxy("http://127.0.0.1:1081")
// NewProxy("http://admin:password@192.168.1.2:8889")
func NewProxy(rawurl string) (*http.Transport, error) {
	if rawurl == "" {
		return new(http.Transport), nil
	}
	u, err := url.Parse(rawurl)
	if err != nil || len(u.String()) <= 10 {
		return new(http.Transport), err
	}
	return &http.Transport{Proxy: http.ProxyURL(u)}, nil
}

// NewTransport 创建一个代理，用于发送请求的时候使用，假如代理链接错误，则返回&http.Transport{},error。
// NewTransport("socks5://127.0.0.1:1080")
// NewTransport("socks5://admin:password@192.168.1.2:8888")
// NewTransport("http://127.0.0.1:1081")
// NewTransport("http://admin:password@192.168.1.2:8889")
func NewTransport(rawurl string) *http.Transport {
	if rawurl == "" {
		return new(http.Transport)
	}
	u, err := url.Parse(rawurl)
	if err != nil || len(u.String()) <= 10 {
		return new(http.Transport)
	}
	return &http.Transport{Proxy: http.ProxyURL(u)}
}

// NewProxyTransport 创建一个代理，用于发送请求的时候使用，假如代理链接有错，则返回nil,error。
// NewProxyTransport("socks5://127.0.0.1:1080")
// NewProxyTransport("socks5://admin:password@192.168.1.2:8888")
// NewProxyTransport("http://127.0.0.1:1081")
// NewProxyTransport("http://admin:password@192.168.1.2:8889")
func NewProxyTransport(rawurl string) (*http.Transport, error) {
	if rawurl == "" {
		return nil, fmt.Errorf("rawUrl == nil")
	}
	u, err := url.Parse(rawurl)
	if err != nil || len(u.String()) <= 10 {
		return nil, fmt.Errorf("rawUrl error: %v", err)
	}
	return &http.Transport{Proxy: http.ProxyURL(u)}, nil
}
