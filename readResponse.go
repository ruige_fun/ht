package ht

import (
	"fmt"
	"io"
	"net/http"
)

// ReadBody 读取响应体，丢弃其它内容。
func ReadBody(resp *http.Response, err error) ([]byte, error) {
	if err != nil {
		return nil, err
	}
	if resp == nil {
		return nil, fmt.Errorf("响应为nil")
	}
	all, err := io.ReadAll(resp.Body)
	_ = resp.Body.Close()
	return all, err
}

// ReadBodyAndStatusCode 读取响应体和响应状态码，丢弃其它内容。
func ReadBodyAndStatusCode(resp *http.Response, err error) ([]byte, int, error) {
	if err != nil {
		return nil, 0, err
	}
	if resp == nil {
		return nil, 0, fmt.Errorf("响应为nil")
	}
	all, err := io.ReadAll(resp.Body)
	_ = resp.Body.Close()
	return all, resp.StatusCode, err
}

// ReadBodyAndHeader 读取响应体和响应头，丢弃其它内容。
func ReadBodyAndHeader(resp *http.Response, err error) ([]byte, http.Header, error) {
	if err != nil {
		return nil, nil, err
	}
	if resp == nil {
		return nil, nil, fmt.Errorf("响应为nil")
	}
	all, err := io.ReadAll(resp.Body)
	_ = resp.Body.Close()
	return all, resp.Header, err
}

// ReadBodyAndHeaderAndStatusCode 读取响应体和响应头、响应状态码，丢弃其它内容。
func ReadBodyAndHeaderAndStatusCode(resp *http.Response, err error) ([]byte, http.Header, int, error) {
	if err != nil {
		return nil, nil, 0, err
	}
	if resp == nil {
		return nil, nil, 0, fmt.Errorf("响应为nil")
	}
	all, err := io.ReadAll(resp.Body)
	_ = resp.Body.Close()
	return all, resp.Header, resp.StatusCode, err
}
