package ht

import "net/http"

var def = NewClient("", 60, "")

// SetUrlLog 设置请求链接打印日志
func SetUrlLog(fx func(a ...any)) {
	def.SetUrlLog(fx)
}

// SetProxy 设置代理
func SetProxy(proxyRaw string) error {
	return def.SetProxy(proxyRaw)
}

// SetTimeout 设置超时时间
func SetTimeout(timeoutSecond uint) {
	def.SetTimeout(timeoutSecond)
}

// SetUserAgent 设置UA信息
func SetUserAgent(ua string) {
	def.SetUserAgent(ua)
}

// Proxy 设置临时代理
// proxyRaw 代理信息，空值则不使用代理；非空："socks5://127.0.0.1:1080" "socks5://admin:password@192.168.1.2:8888"
func Proxy(proxyRaw string) *HttpClient {
	return def.Proxy(proxyRaw)
}

// Timeout 设置临时超时时间
func Timeout(timeoutSecond uint) *HttpClient {
	return def.Timeout(timeoutSecond)
}

// UA 设置临时User-Agent
func UA(ua string) *HttpClient {
	return def.UA(ua)
}

// Httpclient 返回一个http客户端
func Httpclient() *http.Client {
	return def.HttpClient()
}

// Get 发送GET表单请求
func Get(urlRaw string, params RequestParams) (*http.Response, error) {
	return def.GET(urlRaw, params)
}

// GetURL 发送GET表单请求
func GetURL(urlRaw string) (*http.Response, error) {
	return def.GET(urlRaw, NewRequestParams())
}

// Post 发送POST表单请求
func Post(urlRaw string, params RequestParams) (*http.Response, error) {
	return def.POST(urlRaw, params)
}

// Delete 发送DELETE表单请求
func Delete(urlRaw string, params RequestParams) (*http.Response, error) {
	return def.DELETE(urlRaw, params)
}

// DeleteURL 发送DELETE表单请求
func DeleteURL(urlRaw string) (*http.Response, error) {
	return def.DELETE(urlRaw, NewRequestParams())
}

// Put 发送PUT表单请求
func Put(urlRaw string, params RequestParams) (*http.Response, error) {
	return def.PUT(urlRaw, params)
}

// PostJsonMap 发送POST请求，请求体为jsonData和params.Body合并的json编码结果
func PostJsonMap(urlRaw string, params RequestParams, jsonData map[string]any) (*http.Response, error) {
	return def.PostJsonMap(urlRaw, params, jsonData)
}

// PutJsonMap 发送PUT请求，请求体为jsonData和params.Body合并的json编码结果
func PutJsonMap(urlRaw string, params RequestParams, jsonData map[string]any) (*http.Response, error) {
	return def.PutJsonMap(urlRaw, params, jsonData)
}

// PostJsonStruct 发送POST请求，请求体为structPointer和params.Body合并的json编码结果
func PostJsonStruct(urlRaw string, params RequestParams, structPointer any) (*http.Response, error) {
	return def.PostJsonStruct(urlRaw, params, structPointer)
}

// PutJsonStruct 发送PUT请求，请求体为structPointer和params.Body合并的json编码结果
func PutJsonStruct(urlRaw string, params RequestParams, structPointer any) (*http.Response, error) {
	return def.PutJsonStruct(urlRaw, params, structPointer)
}

// PostBody 发送POST请求，请求体为body
func PostBody(urlRaw string, params RequestParams, body []byte, contentType string) (*http.Response, error) {
	return def.PostBody(urlRaw, params, body, contentType)
}

// PutBody 发送PUT请求，请求体为body
func PutBody(urlRaw string, params RequestParams, body []byte, contentType string) (*http.Response, error) {
	return def.PutBody(urlRaw, params, body, contentType)
}
